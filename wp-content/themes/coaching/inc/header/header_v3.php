<?php
$theme_options_data = get_theme_mods();
?>
<!-- <div class="main-menu"> -->
<div class="container">
	<div class="row">
		<div class="col-sm-2 column_logo">
			<div class="tm-table">
				<div class="width-logo table-cell sm-logo">
					<?php
					do_action( 'thim_logo' );
					do_action( 'thim_sticky_logo' );
					?>
				</div>
			</div>
		</div>
		<div class="col-sm-10 column_right">
			<div class="toolbar" id="toolbar">
				<div class="row">
					<div class="col-sm-6 col-xs-12 hidden-sm hidden-xs">
						<?php dynamic_sidebar( 'top_menu_left' ); ?>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12 menu_right_position">
						<?php dynamic_sidebar( 'top_menu_right' ); ?>
					</div>
				</div>
			</div>
			<div class="navigation">
				<nav class="width-navigation table-cell table-right">
					<?php get_template_part( 'inc/header/main-menu' ); ?>
				</nav>
				<div class="menu-mobile-effect navbar-toggle" data-effect="mobile-effect">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</div>
			</div>
		</div>
	</div>
</div>