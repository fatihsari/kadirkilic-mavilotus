<?php
$woocommerce->addSubSection( array(
	'name'     => esc_html__( 'Setting', 'coaching' ),
	'id'       => 'woo_setting',
	'position' => 3,
) );

$woocommerce->createOption( array(
	'name'    => esc_html__( 'Grid', 'coaching' ),
	'id'      => 'woo_product_column',
	'type'    => 'select',
	'options' => array(
		'2' => '2',// column bootstrap
		'3' => '3',
		'4' => '4',
	),
	'default' => '4'
) );
$woocommerce->createOption( array(
	'name'    => esc_html__( 'Number Of Products Per Page', 'coaching' ),
	'id'      => 'woo_product_per_page',
	'type'    => 'number',
	'desc'    => esc_html__( 'Insert the number of posts to display per page.', 'coaching' ),
	'default' => '8',
	'max'     => '100',
) );

$woocommerce->createOption( array(
	'name'    => esc_html__( 'Show QuickView', 'coaching' ),
	'id'      => 'woo_set_show_qv',
	'type'    => 'checkbox',
	'default' => true,
) );
