<?php

$display = $titan->createThimCustomizerSection( array(
	'name'     => esc_html__( 'Display', 'coaching' ),
	'position' => 45,
	'id'       => 'display',
) );