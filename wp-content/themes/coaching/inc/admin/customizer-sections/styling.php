<?php
/*
 * Creating a Styling Options
 */

$styling = $titan->createThimCustomizerSection( array(
	'name'     => esc_html__( 'Styling', 'coaching' ),
	'position' => 40,
	'id'       => 'styling'
) );

