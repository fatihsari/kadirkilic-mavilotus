<?php
$styling->addSubSection( array(
	'name'     => esc_html__( 'Color', 'coaching' ),
	'id'       => 'styling_color',
	'position' => 13,
	'livepreview' => '$("body").css("color", value);'
) );


$styling->createOption( array(
	'name'        => esc_html__( 'Body Background Color', 'coaching' ),
	'id'          => 'body_bg_color',
	'type'        => 'color-opacity',
	'default'     => '#fff',
	'livepreview' => '$("body #main-content").css("background-color", value);'
) );

$styling->createOption( array(
	'name'    => esc_html__( 'Primary Color', 'coaching' ),
	'id'      => 'body_primary_color',
	'type'    => 'color-opacity',
	'default' => '#ffb606',
) );

$styling->createOption( array(
	'name'    => esc_html__( 'Button Hover Color', 'coaching' ),
	'id'      => 'button_hover_color',
	'type'    => 'color-opacity',
	'default' => '#e6a303',
) );
