<?php
$styling->addSubSection( array(
	'name'     => esc_html__( 'Layout', 'coaching' ),
	'id'       => 'styling_layout',
	'position' => 10,
) );
$styling->createOption( array(
	'name'    => esc_html__( 'Body Layout', 'coaching' ),
	'id'      => 'box_layout',
	'type'    => 'select',
	'options' => array(
		'boxed' => esc_html__( 'Boxed', 'coaching' ),
		'wide'  => esc_html__( 'Wide', 'coaching' ),
	),
	'default' => 'Wide',
) );
