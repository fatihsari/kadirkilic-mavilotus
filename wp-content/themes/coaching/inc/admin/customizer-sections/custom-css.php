<?php

$custom_css = $titan->createThemeCustomizerSection( array(
	'name'     => esc_html__( 'Custom CSS', 'coaching' ),
	'position' => 100,
) );

/*
 * Archive Display Settings
 */
$custom_css->createOption( array(
	'name'    => esc_html__( 'Custom CSS', 'coaching' ),
	'id'      => 'custom_css',
	'type'    => 'textarea',
	'desc'    => esc_html__( 'Put your additional CSS rules here.', 'coaching' ),
	'is_code' => true,
) );

$custom_css->createOption( array(
	'name'    => esc_html__( 'Custom JS', 'coaching' ),
	'id'      => 'custom_js',
	'type'    => 'textarea',
	'desc'    => esc_html__( 'Put your additional JS rules here.', 'coaching' ),
	'is_code' => true,
) );