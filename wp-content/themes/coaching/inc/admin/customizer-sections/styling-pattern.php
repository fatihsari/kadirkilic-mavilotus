<?php
$styling->addSubSection( array(
	'name'     => esc_html__( 'Pattern', 'coaching' ),
	'id'       => 'styling_pattern',
	'position' => 11,
) );


$styling->createOption( array(
	'name'    => esc_html__( 'Pattern Background', 'coaching' ),
	'id'      => 'user_bg_pattern',
	'type'    => 'checkbox',
	'desc'    => esc_html__( 'Check the box to display a pattern in the background. If checked, select the pattern from below.', 'coaching' ),
	'default' => false,
) );

$styling->createOption( array(
	'name'    => esc_html__( 'Select Pattern Background', 'coaching' ),
	'id'      => 'bg_pattern',
	'type'    => 'radio-image',
	'options' => array(
		THIM_URI . 'images/patterns/pattern1.png'  => THIM_URI . 'images/patterns/pattern1.png',
		THIM_URI . 'images/patterns/pattern2.png'  => THIM_URI . 'images/patterns/pattern2.png',
		THIM_URI . 'images/patterns/pattern3.png'  => THIM_URI . 'images/patterns/pattern3.png',
		THIM_URI . 'images/patterns/pattern4.png'  => THIM_URI . 'images/patterns/pattern4.png',
		THIM_URI . 'images/patterns/pattern5.png'  => THIM_URI . 'images/patterns/pattern5.png',
		THIM_URI . 'images/patterns/pattern6.png'  => THIM_URI . 'images/patterns/pattern6.png',
		THIM_URI . 'images/patterns/pattern7.png'  => THIM_URI . 'images/patterns/pattern7.png',
		THIM_URI . 'images/patterns/pattern8.png'  => THIM_URI . 'images/patterns/pattern8.png',
		THIM_URI . 'images/patterns/pattern9.png'  => THIM_URI . 'images/patterns/pattern9.png',
		THIM_URI . 'images/patterns/pattern10.png' => THIM_URI . 'images/patterns/pattern10.png',
	)
) );

$styling->createOption( array(
	'name'        => esc_html__( 'Upload Background', 'coaching' ),
	'id'          => 'bg_upload',
	'type'        => 'upload',
	'desc'        => esc_html__( 'Upload your background.', 'coaching' ),
	'livepreview' => ''
) );

$styling->createOption( array(
	'name'    => esc_html__( 'Repeat Background', 'coaching' ),
	'id'      => 'bg_repeat',
	'type'    => 'select',
	'options' => array(
		'repeat'    => 'repeat',
		'repeat-x'  => 'repeat-x',
		'repeat-y'  => 'repeat-y',
		'no-repeat' => 'no-repeat'
	),
	'default' => 'no-repeat'
) );

$styling->createOption( array(
	'name'    => esc_html__( 'Background Position', 'coaching' ),
	'id'      => 'bg_position',
	'type'    => 'select',
	'options' => array(
		'left top'      => esc_html__( 'Left Top', 'coaching' ),
		'left center'   => esc_html__( 'Left Center', 'coaching' ),
		'left bottom'   => esc_html__( 'Left Bottom', 'coaching' ),
		'right top'     => esc_html__( 'Right Top', 'coaching' ),
		'right center'  => esc_html__( 'Right Center', 'coaching' ),
		'right bottom'  => esc_html__( 'Right Bottom', 'coaching' ),
		'center top'    => esc_html__( 'Center Top', 'coaching' ),
		'center center' => esc_html__( 'Center Center', 'coaching' ),
		'center bottom' => esc_html__( 'Center Bottom', 'coaching' )
	),
	'default' => 'center center'
) );

$styling->createOption( array(
	'name'    => esc_html__( 'Background Attachment', 'coaching' ),
	'id'      => 'bg_attachment',
	'type'    => 'select',
	'options' => array(
		'scroll'  => 'scroll',
		'fixed'   => 'fixed',
		'local'   => 'local',
		'initial' => 'initial',
		'inherit' => 'inherit'
	),
	'default' => 'inherit'
) );

$styling->createOption( array(
	'name'    => esc_html__( 'Background Size', 'coaching' ),
	'id'      => 'bg_size',
	'type'    => 'select',
	'options' => array(
		'100% 100%' => '100% 100%',
		'contain'   => 'contain',
		'cover'     => 'cover',
		'inherit'   => 'inherit',
		'initial'   => 'initial'
	),
	'default' => 'inherit'
) );