<?php
$styling->addSubSection( array(
	'name'     => esc_html__( 'Support', 'coaching' ),
	'id'       => 'styling_rtl',
	'position' => 15,
) );

$styling->createOption( array(
	'name'    => esc_html__( 'RTL Support', 'coaching' ),
	'id'      => 'rtl_support',
	'type'    => 'checkbox',
	'desc'    => esc_html__( 'Enable/Disable', 'coaching' ),
	'default' => false,
) );

$styling->createOption( array(
	'name'    => esc_html__( 'Smooth Scroll', 'coaching' ),
	'id'      => 'smooth_scroll',
	'type'    => 'checkbox',
	'desc'    => esc_html__( 'Enable/Disable', 'coaching' ),
	'default' => true,
) );

$styling->createOption( array(
	'name'    => esc_html__( 'Pre-loader', 'coaching' ),
	'id'      => 'preload',
	'type'    => 'checkbox',
	'desc'    => esc_html__( 'Enable/Disable', 'coaching' ),
	'default' => true,
) );

$styling->createOption( array(
	'name'    => esc_html__( 'Pre-loader Image', 'coaching' ),
	'id'      => 'preload_image',
	'type'    => 'upload',
	'desc'    => esc_html__( 'Choose your image pre-loader. Recommend to use .gif format. Blank to display pre-loader default.', 'coaching' ),
) );