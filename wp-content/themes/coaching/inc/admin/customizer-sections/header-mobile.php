<?php

// main menu

$header->addSubSection( array(
	'name'     => esc_html__( 'Mobile Menu', 'coaching' ),
	'id'       => 'display_mobile_menu',
	'position' => 15,
) );


$header->createOption( array(
	'name'    => esc_html__( 'Background Color', 'coaching' ),
	'id'      => 'bg_mobile_menu_color',
	'default' => '#232323',
	'type'    => 'color-opacity'
) );


$header->createOption( array(
	'name'    => esc_html__( 'Text Color', 'coaching' ),
	'id'      => 'mobile_menu_text_color',
	'default' => '#777',
	'type'    => 'color-opacity'
) );
$header->createOption( array(
	'name'    => esc_html__( 'Text Hover Color', 'coaching' ),
	'id'      => 'mobile_menu_text_hover_color',
	'default' => '#fff',
	'type'    => 'color-opacity'
) );

$header->createOption( array(
	'name'    => esc_html__( 'Config Logo', 'coaching' ),
	'desc'    => '',
	'id'      => 'config_logo_mobile',
	'options' => array(
		'default_logo' => esc_html__( 'Default', 'coaching' ),
		'custom_logo'  => esc_html__( 'Custom', 'coaching' ),
	),
	'type'    => 'select',
	'default' => 'default_logo'
) );


$header->createOption( array(
	'name'    => esc_html__( 'Logo', 'coaching' ),
	'id'      => 'logo_mobile',
	'type'    => 'upload',
	'desc'    => esc_html__( 'Upload your logo.', 'coaching' ),
	'default' => THIM_URI . 'images/logo.png',
) );

$header->createOption( array(
	'name'    => esc_html__( 'Sticky Logo', 'coaching' ),
	'id'      => 'sticky_logo_mobile',
	'type'    => 'upload',
	'desc'    => esc_html__( 'Upload your sticky logo.', 'coaching' ),
	'default' => THIM_URI . 'images/logo.png',
) );