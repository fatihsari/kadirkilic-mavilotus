<?php
$data = $titan->createThimCustomizerSection( array(
	'name'     => esc_html__( 'Import/Export', 'coaching' ),
	'desc'     => esc_html__( 'You can export then import settings from one theme to another conveniently without any problem.', 'coaching' ),
	'position' => 200,
	'id'       => 'import_export',
) );

$data->createOption( array(
	'name' => esc_html__( 'Import Settings', 'coaching' ),
	'id'   => 'import_setting',
	'type' => 'customize-import',
	'desc' => esc_html__( 'Click upload button then choose a JSON file (.json) from your computer to import settings to this theme.', 'coaching' ),
) );

$data->createOption( array(
	'name' => esc_html__( 'Export Settings', 'coaching' ),
	'id'   => 'export_setting',
	'type' => 'customize-export',
	'desc' => esc_html__( 'Simply click download button to export all your settings to a JSON file (.json).', 'coaching' ),
) );
