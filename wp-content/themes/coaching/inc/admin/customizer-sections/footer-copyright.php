<?php
$footer->addSubSection( array(
	'name'     => esc_html__( 'Copyright', 'coaching' ),
	'id'       => 'display_copyright',
	'position' => 3,
) );

$footer->createOption( array(
	'name'        => esc_html__( 'Background Color', 'coaching' ),
	'id'          => 'copyright_bg_color',
	'type'        => 'color-opacity',
	'default'     => '#111111',
	'livepreview' => '$("footer#colophon .copyright-area").css("background-color", value);'
) );

$footer->createOption( array(
	'name'        => esc_html__( 'Text Color', 'coaching' ),
	'id'          => 'copyright_text_color',
	'type'        => 'color-opacity',
	'default'     => '#999999',
	'livepreview' => '$("footer#colophon .copyright-area").css("color", value);'
) );

$footer->createOption( array(
	'name'        => esc_html__( 'Copyright Text', 'coaching' ),
	'id'          => 'copyright_text',
	'type'        => 'textarea',
	'default'     =>  sprintf( wp_kses( __( 'Copyright 2016 Coaching WordPress Theme by  <a href="%s">ThimPress</a>.', 'coaching' ),
		array( 'a' => array( 'href' => array() ) ) ), esc_url( 'https://thimpress.com') ),
	'livepreview' => '$("footer#colophon .copyright-area .text-copyright").html(function(){return value ;})'
) );

$footer->createOption( array(
	'name'      => esc_html__( 'Back To Top', 'coaching' ),
	'id'        => 'show_to_top',
	'type'      => 'checkbox',
	'des'       => esc_html__( 'Show or hide this button.', 'coaching' ),
	'default'   => true

) );