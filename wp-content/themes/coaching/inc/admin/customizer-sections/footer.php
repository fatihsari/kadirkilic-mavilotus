<?php
$footer = $titan->createThimCustomizerSection( array(
	'name'     => esc_html__( 'Footer', 'coaching' ),
	'position' => 35,
	'id'       => 'display_footer'
) );
