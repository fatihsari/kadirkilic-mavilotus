<?php

$display->addSubSection( array(
	'name'     => esc_html__( 'Sharing', 'coaching' ),
	'id'       => 'display_sharing',
	'desc'     => esc_html__( 'Show social sharing button.', 'coaching' ),
	'position' => 6,
) );

$display->createOption( array(
	'name'    => esc_html__('Facebook', 'coaching'),
	'id'      => 'sharing_facebook',
	'type'    => 'checkbox',
	'default' => true,
) );

$display->createOption( array(
	'name'    => esc_html__('Twitter', 'coaching'),
	'id'      => 'sharing_twitter',
	'type'    => 'checkbox',
	'default' => true,
) );

$display->createOption( array(
	'name'    => esc_html__('Google Plus', 'coaching'),
	'id'      => 'sharing_google',
	'type'    => 'checkbox',
	'default' => true,
) );

$display->createOption( array(
	'name'    => esc_html__('Pinterest', 'coaching'),
	'id'      => 'sharing_pinterest',
	'type'    => 'checkbox',
	'default' => true,
) );

