<?php
/*
 * Post and Page Display Settings
 */
$display->addSubSection( array(
	'name'     => esc_html__( 'Archive', 'coaching' ),
	'id'       => 'display_archive',
	'position' => 2,
) );

$display->createOption( array(
	'name'    => esc_html__( 'Archive Layout', 'coaching' ),
	'id'      => 'archive_cate_layout',
	'type'    => 'radio-image',
	'options' => array(
		'full-content'  => THIM_URI . 'images/admin/layout/body-full.png',
		'sidebar-left'  => THIM_URI . 'images/admin/layout/sidebar-left.png',
		'sidebar-right' => THIM_URI . 'images/admin/layout/sidebar-right.png'
	),
	'default' => 'sidebar-right'
) );

$display->createOption( array(
	'name'    => esc_html__( 'Hide Breadcrumbs', 'coaching' ),
	'id'      => 'archive_cate_hide_breadcrumbs',
	'type'    => 'checkbox',
	'desc'    => esc_html__( 'Check this box to hide/show breadcrumbs.', 'coaching' ),
	'default' => false,
) );

$display->createOption( array(
	'name'    => esc_html__( 'Hide Title', 'coaching' ),
	'id'      => 'archive_cate_hide_title',
	'type'    => 'checkbox',
	'desc'    => esc_html__( 'Check this box to hide/show title.', 'coaching' ),
	'default' => false,
) );

$display->createOption( array(
	'name'        => esc_html__( 'Top Image', 'coaching' ),
	'id'          => 'archive_cate_top_image',
	'type'        => 'upload',
	'desc'        => esc_html__( 'Enter URL or upload a top image file for header.', 'coaching' ),
	'default'     => THIM_URI . 'images/bg-page-blog.jpg',
	'livepreview' => ''
) );

$display->createOption( array(
	'name'        => esc_html__( 'Heading Background Color', 'coaching' ),
	'id'          => 'archive_cate_heading_bg_color',
	'type'        => 'color-opacity',
	'livepreview' => ''
) );

$display->createOption( array(
	'name'    => esc_html__( 'Heading Text Color', 'coaching' ),
	'id'      => 'archive_cate_heading_text_color',
	'type'    => 'color-opacity',
	'default' => '#fff',
) );

$display->createOption( array(
	'name'    => esc_html__( 'Sub Heading Text Color', 'coaching' ),
	'id'      => 'archive_cate_sub_heading_text_color',
	'type'    => 'color-opacity',
	'default' => '#878787',
) );

$display->createOption( array(
	'name'    => esc_html__( 'Sub Title', 'coaching' ),
	'id'      => 'archive_cate_sub_title',
	'type'    => 'text',
	'default' => '',
) );

$display->createOption( array(
	'name'    => esc_html__( 'Excerpt Length', 'coaching' ),
	'id'      => 'archive_excerpt_length',
	'type'    => 'number',
	'desc'    => esc_html__( 'Enter the number of words you want to cut from the content to be the excerpt of search and archive and portfolio page.', 'coaching' ),
	'default' => '40',
	'max'     => '100',
	'min'     => '10',
) );


$display->createOption( array(
	'name'    => esc_html__( 'Show Category', 'coaching' ),
	'id'      => 'show_category',
	'type'    => 'checkbox',
	'desc'    => esc_html__( 'show/hidden', 'coaching' ),
	'default' => true,
) );

$display->createOption( array(
	'name'    => esc_html__( 'Show Date', 'coaching' ),
	'id'      => 'show_date',
	'type'    => 'checkbox',
	'desc'    => esc_html__( 'show/hidden', 'coaching' ),
	'default' => true,
) );

$display->createOption( array(
	'name'    => esc_html__( 'Show Comment', 'coaching' ),
	'id'      => 'show_comment',
	'type'    => 'checkbox',
	'desc'    => esc_html__( 'show/hidden', 'coaching' ),
	'default' => true,
) );