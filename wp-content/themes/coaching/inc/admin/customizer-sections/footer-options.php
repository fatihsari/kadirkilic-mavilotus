<?php

$footer->addSubSection( array(
	'name'     => esc_html__( 'Footer', 'coaching' ),
	'id'       => 'display_footer',
	'position' => 2,
) );
$footer->createOption( array(
	'name'    => esc_html__( 'Footer Title Color', 'coaching' ),
	'id'      => 'footer_title_font_color',
	'type'    => 'color-opacity',
	'default' => '#fff',
	'livepreview' => '$("footer#colophon .footer .widget-title").css("color", value);'
) );
$footer->createOption( array(
	'name'    => esc_html__( 'Footer Text Color', 'coaching' ),
	'id'      => 'footer_text_font_color',
	'type'    => 'color-opacity',
	'default' => '#fff',
	'livepreview' => '$("footer#colophon .footer").css("color", value);'
) );

$footer->createOption( array(
	'name'        => esc_html__( 'Background Color', 'coaching' ),
	'id'          => 'footer_bg_color',
	'type'        => 'color-opacity',
	'default'     => '#111111',
	'livepreview' => '$("footer#colophon").css("background-color", value);'
) );

$footer->createOption( array(
	'name' => esc_html__( 'Background Image', 'coaching' ),
	'id'   => 'footer_background_img',
	'type' => 'upload',
) );

$footer->createOption( array(
	'name'    => esc_html__( 'Background Position', 'coaching' ),
	'id'      => 'footer_bg_position',
	'type'    => 'select',
	'options' => array(
		'left top'      => esc_html__( 'Left Top', 'coaching' ),
		'left center'   => esc_html__( 'Left Center', 'coaching' ),
		'left bottom'   => esc_html__( 'Left Bottom', 'coaching' ),
		'right top'     => esc_html__( 'Right Top', 'coaching' ),
		'right center'  => esc_html__( 'Right Center', 'coaching' ),
		'right bottom'  => esc_html__( 'Right Bottom', 'coaching' ),
		'center top'    => esc_html__( 'Center Top', 'coaching' ),
		'center center' => esc_html__( 'Center Center', 'coaching' ),
		'center bottom' => esc_html__( 'Center Bottom', 'coaching' )
	),
	'default' => 'center center'
) );