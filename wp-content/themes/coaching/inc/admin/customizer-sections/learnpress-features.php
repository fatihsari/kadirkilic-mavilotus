<?php
$courses->addSubSection( array(
	'name'     => esc_html__( 'Features', 'coaching' ),
	'id'       => 'learnpress_features',
	'position' => 5,
) );

$courses->createOption( array(
	'name'    => esc_html__( 'Hidden Ads', 'coaching' ),
	'id'      => 'learnpress_hidden_ads',
	'type'    => 'checkbox',
	'desc'    => esc_html__( 'Hidden ads learnpress on WordPress admin.', 'coaching' ),
	'default' => false,
) );