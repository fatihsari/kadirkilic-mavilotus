<?php


$header->addSubSection( array(
	'name'     => esc_html__( 'Toolbar', 'coaching' ),
	'id'       => 'display_toolbar',
	'position' => 3,
) );


$header->createOption( array(
	'name'    => esc_html__( 'Show / Hide Toolbar', 'coaching' ),
	'id'      => 'toolbar_show',
	'type'    => 'checkbox',
	'default' => true,

) );

$header->createOption( array(
	'name'    => esc_html__( 'Font Size', 'coaching' ),
	'id'      => 'font_size_toolbar',
	'type'    => 'select',
	'options' => $font_sizes,
	'default' => '12px',
	'livepreview' => '$("#toolbar").css("fontSize", value);'
) );

$header->createOption( array(
	'name'        => esc_html__( 'Background Color', 'coaching' ),
	'id'          => 'bg_color_toolbar',
	'type'        => 'color-opacity',
	'default'     => 'rgba(0,0,0,0)',
	'livepreview' => '$("#toolbar").css("background-color", value);'
) );

$header->createOption( array(
	'name'    => esc_html__( 'Text Color', 'coaching' ),
	'id'      => 'text_color_toolbar',
	'type'    => 'color-opacity',
	'default' => '#fff',
	'livepreview' => '
		$("#toolbar").css("color", value);
		'
) );

$header->createOption( array(
	'name'    => esc_html__( 'Link Color', 'coaching' ),
	'id'      => 'link_color_toolbar',
	'type'    => 'color-opacity',
	'default' => '#fff',
	'livepreview' => '$("#toolbar a").css("color", value);'
) );

