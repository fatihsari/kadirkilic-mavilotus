<?php

$woocommerce = $titan->createThimCustomizerSection( array(
	'name'     => esc_html__( 'WooCommerce', 'coaching' ),
	'position' => 70,
	'id'       => 'woocommerce',
) );