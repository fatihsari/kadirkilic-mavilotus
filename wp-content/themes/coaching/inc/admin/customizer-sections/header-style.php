<?php
$header->addSubSection( array(
	'name'     => esc_html__( 'Style', 'coaching' ),
	'id'       => 'display_menu_style',
	'position' => 2,
) );

$header->createOption( array(
	'name'    => esc_html__( 'Header Style', 'coaching' ),
	'id'      => 'header_style',
	'type'    => 'select',
	'options' => array(
		'header_v1' => 'Life Style',
		'header_v2' => 'Health Style 1',
		'header_v3' => 'Health Style 2',
		'header_v4' => 'Health Style 3',
	),
	'default' => 'header_v1',
) );