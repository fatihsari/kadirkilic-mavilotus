<?php

defined( 'DS' ) OR define( 'DS', DIRECTORY_SEPARATOR );

$demo_datas_dir = THIM_DIR . 'inc' . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR . 'data';

$demo_datas = array(
	'demo-01' => array(
		'data_dir'      => $demo_datas_dir . DS . 'demo-01',
		'thumbnail_url' => THIM_URI . 'inc/admin/data/demo-01/screenshot.jpg',
		'title'         => esc_html__( 'Demo 01', 'coaching' ),
		'demo_url'      => 'http://coaching.thimpress.com/',
	),
	'demo-02' => array(
		'data_dir'      => $demo_datas_dir . DS . 'demo-02',
		'thumbnail_url' => THIM_URI . 'inc/admin/data/demo-02/screenshot.jpg',
		'title'         => esc_html__( 'Demo 02', 'coaching' ),
		'demo_url'      => 'http://coaching.thimpress.com/demo-health-coaching/',
	),
	'demo-03' => array(
		'data_dir'      => $demo_datas_dir . DS . 'demo-03',
		'thumbnail_url' => THIM_URI . 'inc/admin/data/demo-03/screenshot.jpg',
		'title'         => esc_html__( 'Demo 03', 'coaching' ),
		'demo_url'      => 'http://coaching.thimpress.com/demo-health-coaching-2/',
	),
	'demo-04' => array(
		'data_dir'      => $demo_datas_dir . DS . 'demo-04',
		'thumbnail_url' => THIM_URI . 'inc/admin/data/demo-04/screenshot.jpg',
		'title'         => esc_html__( 'Demo 04', 'coaching' ),
		'demo_url'      => 'http://coaching.thimpress.com/demo-health-coaching-3/',
	),
	'demo-rtl' => array(
		'data_dir'      => $demo_datas_dir . DS . 'demo-rtl',
		'thumbnail_url' => THIM_URI . 'inc/admin/data/demo-rtl/screenshot.jpg',
		'title'         => esc_html__( 'Demo RTL', 'coaching' ),
		'demo_url'      => 'http://coaching.thimpress.com/demo-rtl/',
	)
);