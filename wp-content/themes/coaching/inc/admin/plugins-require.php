<?php
/**
 * Include the TGM_Plugin_Activation class.
 */
add_action( 'tgmpa_register', 'thim_tp_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */

if ( !function_exists( 'thim_tp_register_required_plugins' ) ) {
	function thim_tp_register_required_plugins() {
		$plugins = array(
			array(
				'name'               => esc_html__( 'Thim Framework', 'coaching' ),
				// The plugin name
				'slug'               => 'thim-framework',
				// The plugin slug (typically the folder name)
				'source'             => 'https://plugins.thimpress.com/downloads/thim-framework.zip',
				// The plugin source
				'required'           => true,
				// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation'   => false,
				// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' => false,
				// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url'       => '',
				// If set, overrides default API URL and points to an external URL
			),

			array(
				'name'     => esc_html__( 'SiteOrigin Page Builder', 'coaching' ),
				'slug'     => 'siteorigin-panels',
				'required' => true,
			),

			array(
				'name'     => esc_html__( 'Black Studio TinyMCE Widget', 'coaching' ),
				'slug'     => 'black-studio-tinymce-widget',
				'required' => false,
			),
			array(
				'name'     => esc_html__( 'Contact Form 7', 'coaching' ),
				'slug'     => 'contact-form-7',
				'required' => false
			),
			array(
				'name'     => esc_html__( 'MailChimp for WordPress', 'coaching' ),
				'slug'     => 'mailchimp-for-wp',
				'required' => false
			),
			array(
				'name'     => esc_html__( 'WooCommerce', 'coaching' ),
				'slug'     => 'woocommerce',
				'required' => false,
			),
			array(
				'name'     => esc_html__( 'Widget Logic', 'coaching' ),
				'slug'     => 'widget-logic',
				'required' => false,
			),
			array(
				'name'     => 'Social Login',
				'slug'     => 'miniorange-login-openid',
				'required' => false,
			),
			array(
				'name'     => esc_html__( 'Social Count Plus', 'coaching' ),
				'slug'     => 'social-count-plus',
				'required' => false,
			),

			array(
				'name'               => esc_html__( 'Thim Twitter', 'coaching' ),
				// The plugin name
				'slug'               => 'thim-twitter',
				// The plugin slug (typically the folder name)
				'source'             => 'https://plugins.thimpress.com/downloads/thim-twitter.zip',
				// The plugin source
				'required'           => false,
				// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation'   => false,
				// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' => false,
				// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url'       => '',
				// If set, overrides default API URL and points to an external URL
			),

			array(
				'name'     => esc_html__( 'WP Events Manager', 'coaching' ),
				'slug'     => 'wp-events-manager',
				'required' => false,
			),

			array(
				'name'               => esc_html__( 'WP Events Manager Woo Payment', 'coaching' ),
				// The plugin name
				'slug'               => 'wp-events-manager-woo-payment',
				// The plugin slug (typically the folder name)
				'source'             => 'https://plugins.thimpress.com/downloads/eduma-plugins/wp-events-manager-woo-payment.zip',
				// The plugin source
				'required'           => false,
				// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation'   => false,
				// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' => false,
				// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url'       => '',
				// If set, overrides default API URL and points to an external URL
			),

			array(
				'name'               => esc_html__( 'Thim Portfolio', 'coaching' ),
				// The plugin name

				'slug'               => 'tp-portfolio',
				// The plugin slug (typically the folder name)
				'source'             => 'https://plugins.thimpress.com/downloads/tp-portfolio.zip',
				// The plugin source
				'required'           => false,
				// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation'   => false,
				// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' => false,
				// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url'       => '',
				// If set, overrides default API URL and points to an external URL
			),

			array(
				'name'               => esc_html__( 'Thim Our Team', 'coaching' ),
				// The plugin name
				'slug'               => 'thim-our-team',
				// The plugin slug (typically the folder name)
				'source'             => 'https://plugins.thimpress.com/downloads/thim-our-team.zip',
				// The plugin source
				'required'           => false,
				// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation'   => false,
				// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' => false,
				// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url'       => '',
				// If set, overrides default API URL and points to an external URL
			),
			array(
				'name'               => esc_html__( 'Thim Testimonials', 'coaching' ),
				// The plugin name
				'slug'               => 'thim-testimonials',
				// The plugin slug (typically the folder name)
				'source'             => 'https://plugins.thimpress.com/downloads/thim-testimonials.zip',
				// The plugin source
				'required'           => false,
				// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation'   => false,
				// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' => false,
				// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url'       => '',
				// If set, overrides default API URL and points to an external URL
			),

			array(
				'name'     => esc_html__( 'LearnPress', 'coaching' ), // The plugin name
				'slug'     => 'learnpress', // The plugin slug (typically the folder name)
				'required' => true, // If false, the plugin is only 'recommended' instead of required
			),

			array(
				'name'               => esc_html__( 'LearnPress Certificates', 'coaching' ),
				// The plugin name
				'slug'               => 'learnpress-certificates',
				// The plugin slug (typically the folder name)
				'source'             => 'https://plugins.thimpress.com/downloads/eduma-plugins/learnpress-certificates.zip',
				// The plugin source
				'required'           => false,
				// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation'   => false,
				// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' => false,
				// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url'       => '',
				// If set, overrides default API URL and points to an external URL
			),

			array(
				'name'     => esc_html__( 'LearnPress Course Review', 'coaching' ),
				'slug'     => 'learnpress-course-review',
				'required' => false,
			),
			array(
				'name'     => esc_html__( 'LearnPress Wishlist', 'coaching' ),
				'slug'     => 'learnpress-wishlist',
				'required' => false,
			),

			array(
				'name'               => esc_html__( 'Pricing Table Plus', 'coaching' ),
				// The plugin name
				'slug'               => 'pricing-table-plus',
				// The plugin slug (typically the folder name)
				'source'             => 'https://plugins.thimpress.com/downloads/pricing-table-plus.zip',
				// The plugin source
				'required'           => true,
				// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation'   => false,
				// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' => false,
				// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url'       => '',
				// If set, overrides default API URL and points to an external URL
			),

			array(
				'name'               => esc_html__( 'Slider Revolution', 'coaching' ),
				// The plugin name
				'slug'               => 'revslider',
				// The plugin slug (typically the folder name)
				'source'             => 'https://plugins.thimpress.com/downloads/revslider.zip',
				// The plugin source
				'required'           => true,
				// If false, the plugin is only 'recommended' instead of required
				'version'            => '5.4.3.1',
				// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation'   => false,
				// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' => false,
				// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url'       => '',
				// If set, overrides default API URL and points to an external URL
			),

		);

		/**
		 * Array of configuration settings. Amend each line as needed.
		 * If you want the default strings to be available under your own theme domain,
		 * leave the strings uncommented.
		 * Some of the strings are added into a sprintf, so see the comments at the
		 * end of each line for what each argument will be.
		 */
		$config = array(
			'domain'       => 'coaching', // Text domain - likely want to be the same as your theme.
			'default_path' => '', // Default absolute path to pre-packaged plugins
			'parent_slug'  => 'themes.php', // Default parent menu slug
			'menu'         => 'install-required-plugins', // Menu slug
			'has_notices'  => true, // Show admin notices or not
			'is_automatic' => true, // Automatically activate plugins after installation or not
			'message'      => '', // Message to output right before the plugins table
			'strings'      => array(
				'page_title'                      => esc_html__( 'Install Required Plugins', 'coaching' ),
				'menu_title'                      => esc_html__( 'Install Plugins', 'coaching' ),
				'installing'                      => esc_html__( 'Installing Plugin: %s', 'coaching' ),
				// %1$s = plugin name
				'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'coaching' ),
				'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'coaching' ),
				// %1$s = plugin name(s)
				'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'coaching' ),
				// %1$s = plugin name(s)
				'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'coaching' ),
				// %1$s = plugin name(s)
				'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'coaching' ),
				// %1$s = plugin name(s)
				'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'coaching' ),
				// %1$s = plugin name(s)
				'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'coaching' ),
				// %1$s = plugin name(s)
				'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'coaching' ),
				// %1$s = plugin name(s)
				'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'coaching' ),
				// %1$s = plugin name(s)
				'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'coaching' ),
				'activate_link'                   => _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'coaching' ),
				'return'                          => esc_html__( 'Return to Required Plugins Installer', 'coaching' ),
				'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'coaching' ),
				'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'coaching' ),
				// %1$s = dashboard link
				'nag_type'                        => 'updated'
				// Determines admin notice type - can only be 'updated' or 'error'
			)
		);
		tgmpa( $plugins, $config );
	}
}

