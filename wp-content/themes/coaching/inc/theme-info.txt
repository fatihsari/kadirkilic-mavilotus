/*
Theme Name: Coaching
Theme URI: https://coaching.thimpress.com/
Author: ThimPress
Author URI: https://thimpress.com
Description: WordPress Theme for Coaching
Version: 1.7.0
License: Split License
License URI: https://help.market.envato.com/hc/en-us/articles/202501064-What-is-Split-Licensing-and-the-GPL-
Tags: two-columns, three-columns, left-sidebar, right-sidebar, custom-background, custom-header, custom-menu, editor-style, post-formats, rtl-language-support, sticky-post, theme-options, translation-ready, accessibility-ready
Text Domain: coaching
Domain Path: /languages/
*/
