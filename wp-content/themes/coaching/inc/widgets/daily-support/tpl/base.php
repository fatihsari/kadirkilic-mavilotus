<?php

$title    = $instance['title'] ? $instance['title'] : '';
$daily_support = $instance['daily-support'] ? $instance['daily-support'] : '';
if ( empty( $daily_support ) ) {
	return;
}
?>

<div class="thim_daily_support_container">
	<div class="thim_daily_support">

		<?php foreach ( $daily_support as $key => $support )  : ?>
			<?php
			$support_image = $support['image_support'];
			$position_support  = $support['position_support'];
			$body_support = $support['body_support'];
			$img = wp_get_attachment_image_src( $support_image, 'full' );
			?>
			<div class="thim_item_support <?php echo esc_html($position_support);?>">
				<?php if($img[0]) {?>
				<img src="<?php echo $img[0];?>" alt="">
				<?php }?>
				<div class="thim_content_support">
					<?php echo ent2ncr( $body_support );?>
				</div>
			</div>
		<?php endforeach ?>
	</div>
</div>




