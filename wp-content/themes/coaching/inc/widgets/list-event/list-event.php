<?php

class Thim_List_Event_Widget extends Thim_Widget {

    function grilabs__get_event_cats() {
        global $wpdb;
        $query = $wpdb->get_results( $wpdb->prepare(
            "
				  SELECT      t1.term_id, t2.name
				  FROM        $wpdb->term_taxonomy AS t1
				  INNER JOIN $wpdb->terms AS t2 ON t1.term_id = t2.term_id
				  WHERE t1.taxonomy = %s
				  ",
            'tp_event_category'
        ) );

        $cats        = array();
        $cats['all'] = 'Tümü';
        if ( ! empty( $query ) ) {
            foreach ( $query as $key => $value ) {
                $cats[ $value->term_id ] = $value->name;
            }
        }

        return $cats;
    }


	function __construct() {

		parent::__construct(
			'list-event',
			esc_html__( 'Thim: List Events ', 'coaching' ),
			array(
				'description'   => esc_html__( 'Display list events', 'coaching' ),
				'help'          => '',
				'panels_groups' => array( 'thim_widget_group' ),
			),
			array(),
			array(
				'title'         => array(
					'type'                  => 'text',
					'label'                 => esc_html__( 'Title', 'coaching' ),
					'allow_html_formatting' => true
				),
				'layout'        => array(
					'type'    => 'select',
					'label'   => esc_html__( 'Layout', 'coaching' ),
					'options' => array(
						'base'     => esc_html__( 'Default', 'coaching' ),
						'layout-business' => esc_html__( 'Layout Business', 'coaching' ),
						'layout-2' => esc_html__( 'Layout 2', 'coaching' ),
					),
					'default' => 'base'
				),
				'source'        => array(
					'type'    => 'select',
					'label'   => esc_html__( 'İçerik Kaynağı', 'coaching' ),
					'options' => array(
						'default'     => esc_html__( 'Süresi Dolanları Gizle', 'coaching' ),
						'category' => esc_html__( 'Belirli Bir Kategoriden Getir', 'coaching' )
					),
					'default' => 'default'
				),

                'category' => [
				    'type' => 'select',
                    'label' => 'Kategori Belirleyin',
                    'options' => $this->grilabs__get_event_cats(),
                    'default' => '0'
                ],

				'number_posts'  => array(
					'type'    => 'number',
					'label'   => esc_html__( 'Number posts', 'coaching' ),
					'default' => '2'
				),
				'feature_items' => array(
					'type'    => 'number',
					'label'   => esc_html__( 'Feature Items', 'coaching' ),
					'default' => '1'
				),
				'text_link'     => array(
					'type'                  => 'text',
					'label'                 => esc_html__( 'Text Link All', 'coaching' ),
					'default'               => esc_html__( 'View All', 'coaching' ),
					'allow_html_formatting' => true
				),
				'url_link'         => array(
					'type'                  => 'text',
					'label'                 => esc_html__( 'Url Link All', 'coaching' ),
					'allow_html_formatting' => true
				),

			),
			THIM_DIR . 'inc/widgets/list-event/'
		);
	}

	/**
	 * Initialize the CTA widget
	 */

	function get_template_name( $instance ) {
		if ( isset( $instance['layout'] ) ) {
			return $instance['layout'];
		} else {
			return 'base';
		}

	}

	function get_style_name( $instance ) {
		return false;
	}
}

function thim_list_event_widget() {
	register_widget( 'Thim_List_Event_Widget' );
}

add_action( 'widgets_init', 'thim_list_event_widget' );