<?php
$_wp_query = $GLOBALS["thim_happening_events"];
?>
	<div class="event-archive-happening active">
		<?php while ( $_wp_query->have_posts() ) : $_wp_query->the_post(); ?>

			<?php get_template_part( 'tp-event/content', 'event' ); ?>

		<?php endwhile; ?>
	</div>
<?php wp_reset_postdata(); ?>