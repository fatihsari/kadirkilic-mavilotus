<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php if ( tp_event_location() ): ?>
	<div class="entry-location">
		<?php
	if ( function_exists( 'tp_event_get_location_map' ) ) {
		tp_event_get_location_map();
	}?>
	</div>
<?php endif; ?>