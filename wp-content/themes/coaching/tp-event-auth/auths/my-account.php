<?php
/**
 * @Author: ducnvtt
 * @Date  :   2016-02-19 09:11:59
 * @Last  Modified by:   ducnvtt
 * @Last  Modified time: 2016-03-07 15:58:53
 */
use TP_Event_Auth\Books\Book as Book;

if ( !defined( 'ABSPATH' ) ) {
	exit;
}

?>

<div class="account-event">
	<?php do_action( 'event_auth_messages', $atts ); ?>
</div>

<?php
if ( !is_user_logged_in() ) {
	printf( wp_kses( __( 'You are not <a href="%s">login</a>', 'coaching' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( event_auth_login_url() ) );
	return;
}

if ( $atts->have_posts() ) : ?>

	<table class="list-book-event">
		<thead>
		<th class="id"><?php esc_html_e( 'ID', 'coaching' ); ?></th>
		<th><?php esc_html_e( 'Etkinlikler', 'coaching' ); ?></th>
		<th class="type"><?php esc_html_e( 'Type', 'coaching' ); ?></th>
		<th><?php esc_html_e( 'Cost', 'coaching' ); ?></th>
		<th class="quantity"><?php esc_html_e( 'Quantity', 'coaching' ); ?></th>
		<th class="method"><?php esc_html_e( 'Method', 'coaching' ); ?></th>
		<th><?php esc_html_e( 'Status', 'coaching' ); ?></th>
		</thead>
		<tbody>
		<?php foreach ( $atts->posts as $post ): ?>

			<?php $booking = Book::instance( $post->ID ) ?>
			<tr>
				<td class="id"><?php printf( '%s', event_auth_format_ID( $post->ID ) ) ?></td>
				<td><?php printf( '<a href="%s">%s</a>', get_the_permalink( $booking->event_id ), get_the_title( $booking->event_id ) ) ?></td>
				<td class="type"><?php printf( '%s', floatval( $booking->cost ) == 0 ? esc_html__( 'Free', 'coaching' ) : esc_html__( 'Cost', 'coaching' ) ) ?></td>
				<td><?php printf( '%s', event_auth_format_price( floatval( $booking->cost ), $booking->currency ) ) ?></td>
				<td class="quantity"><?php printf( '%s', $booking->qty ) ?></td>
				<td class="method"><?php printf( '%s', $booking->payment_id ? event_auth_get_payment_title( $booking->payment_id ) : esc_html__( 'No payment.', 'coaching' ) ) ?></td>
				<td><?php printf( '%s', event_auth_booking_status( $booking->ID ) ); ?></td>
			</tr>

		<?php endforeach; ?>
		</tbody>
	</table>

<?php endif; ?>
