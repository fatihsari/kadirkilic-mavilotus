<?php
$_wp_query = $GLOBALS["thim_happening_events"];
?>
	<div class="event-archive-happening active">
		<?php while ( $_wp_query->have_posts() ) : $_wp_query->the_post(); ?>

			<?php get_template_part( 'wp-events-manager/content', 'event' ); ?>

		<?php endwhile; ?>
	</div>
<?php wp_reset_postdata(); ?>